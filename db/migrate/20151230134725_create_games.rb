class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :grid
      t.boolean :player_number, :default => true
      t.integer :game_status
      t.timestamps null: false
    end
  end
end
