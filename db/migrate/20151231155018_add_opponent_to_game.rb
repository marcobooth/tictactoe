class AddOpponentToGame < ActiveRecord::Migration
  def change
    add_column :games, :opponent, :integer
  end
end
