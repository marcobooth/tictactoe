class Game < ActiveRecord::Base
  belongs_to :user
  
  after_initialize :convert_from_string
  before_save :convert_to_string

  def convert_from_string
    if (self.grid.nil?)
      grid = Grid.new
      @initial_grid = grid
      self.game_status = 0
    else
      @initial_grid = Grid.from_s(self.grid)
    end
  end

  def convert_to_string
    self.grid = @initial_grid.to_s
  end

  def initial_grid
    @initial_grid
  end

  def check_status
    return initial_grid.check_game_status(self.player_number)
  end

end
