class User < ActiveRecord::Base
  before_save :downcase_email
  validates :email, presence: true
  has_many :games
  has_secure_password

  private
    def downcase_email
      self.email = email.downcase
    end
end
