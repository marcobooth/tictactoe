class Grid
  def initialize(array = ['_', '_', '_', '_', '_', '_', '_', '_', '_'])
    @numbers = array
  end

  def to_s
    return @numbers.join(",")
  end

  def self.from_s(string)
    array = string.split(",")
    return Grid.new(array)
  end

  def numbers
    @numbers
  end

  def check_game_status(current_player)
    @player = current_player == true ? 'X' : 'O'

    # check rows
    $k = 0
    while $k < 8
      for i in 0..2
        if (@numbers[$k + i] == @player)
          if (i == 2)
            return 1
          end
        else
          break
        end
      end
      $k += 3
    end

    # check columns
    $k = 0
    for i in 0..2
      while $k < 8
        if (@numbers[$k + i] == @player)
          if ($k > 5)
            return 1
          end
          $k += 3
        else
          $k = 0
          break
        end
      end
    end

    # check diagonals
    if (@numbers[2] == @player && @numbers[4] == @player && numbers[6] == @player)
      return 1
    elsif (@numbers[0] == @player && @numbers[4] == @player && numbers[8] == @player)
      return 1
    end

    # check if game is draw (all spaces have been used)
    for i in 0..8
      if (@numbers[i] == "_")
        break
      elsif i == 8
        return 2
      end
    end

    # game continues if none of the above conditions have been met
    return 0
          

  end

end
