class GamesController < ApplicationController
  
  before_action :require_login, :only => [:create, :join, :edit, :change]
  
  def index
    @games = Game.where("opponent IS NULL ")
  end

  def create
    @game = current_user.games.build

    if @game.save
      redirect_to edit_game_path(@game)
    else
      redirect_to :back
    end
  end

  def join
    @games = Game.all
    @game = Game.find(params[:id])

    if check_opponent(@games, @game) == 0 && @game.update_attribute(:opponent, current_user.id)
      redirect_to edit_game_path(@game)
    else
      flash[:danger] = "Already in game with this player"
      redirect_to root_path
    end
  end

  def edit
    @game = Game.find(params[:id])
  end

  def show
    @game = Game.find(params[:id])
  end

  def change
    @game = Game.find(params[:id])

    #finds current player and changes the chosen position to an X or O
    @player = @game.player_number == true ? 'X' : 'O'
    @game.initial_grid.numbers[params[:number].to_i] = @player
    
    @status = @game.check_status

    if @game.save
      # checks if game is finished (win or draw), otherwise changes player and goes back to game
      if @status == 1 || @status == 2
        @game.update_column(:game_status, @status)
        redirect_to game_path(params[:id])
      else
        @game.player_number == true ? @game.update_column(:player_number, false) : @game.update_column(:player_number, true)
        redirect_to edit_game_path(params[:id])
      end
    else
      redirect_to root_path
    end
  end

  private

    def check_opponent(games, requested_game)
      @list_current_opponents = []
      # finds all current opponents, those who have joined your games or vice versa
      games.each do |game|
        if game.user_id == current_user.id && game.game_status == 0
          @list_current_opponents.push(game.opponent)
        end
        if game.opponent == current_user.id && game.game_status == 0
          @list_current_opponents.push(game.user_id)
        end
      end
      @new_opponent = 0
      # checks current opponents are not in the game you are about to join
      @list_current_opponents.each do |id|
        if id == requested_game.user_id
          @new_opponent = 1
        end
      end
      return @new_opponent
    end

end
